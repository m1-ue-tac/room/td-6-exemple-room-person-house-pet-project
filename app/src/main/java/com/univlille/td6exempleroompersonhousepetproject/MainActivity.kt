package com.univlille.td6exempleroompersonhousepetproject

import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.univlille.td6exempleroompersonhousepetproject.data.entity.House
import com.univlille.td6exempleroompersonhousepetproject.data.entity.Person
import com.univlille.td6exempleroompersonhousepetproject.data.entity.Pet
import com.univlille.td6exempleroompersonhousepetproject.data.entity.Project
import com.univlille.td6exempleroompersonhousepetproject.data.model.PersonAndHouse
import com.univlille.td6exempleroompersonhousepetproject.data.model.PersonAndPets
import com.univlille.td6exempleroompersonhousepetproject.data.model.PersonAndProjects

class MainActivity : AppCompatActivity() {
    /**
     * Objectif : faire une BDD qui contient des Personnes qui ont :
     * - 1 maison
     * - 1 ou plusieurs animaux
     * - 1 ou plusieurs projets (qui peuvent être associés à plusieurs personnes)
     */
    // le ViewModel (ici, un AndroidViewModel pour gérer + facilement le Contexte nécessaire à Room)
    private var mainActivityViewModel: MainActivityViewModel? = null

    // compteur utilisé uniquement pour des tests de création de data
    private var compteur = 0

    // zone de texte utilisée pour l'affichage des résultats
    private var listeDesPersonnes: TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // définition des widgets et de leur comportement

        // la zone de texte des résultats
        listeDesPersonnes = findViewById(R.id.resultat)

        // bouton permettant de tester l'ajout de nouvelles données dans la BDD.
        // Pour rappel, et pour les tests, la BDD est détruite à chaque lancement...
        // (Cf. MaDataBase pour les explications)
        val btnAjouterPersonne = findViewById<Button>(R.id.btnAjouterPersonne)
        btnAjouterPersonne.setOnClickListener {
            compteur++
            //                ajouterPersonneEtProjets("Personne " + compteur);
            ajouterPersonne("Personne $compteur")
        }
        val btnSearch = findViewById<Button>(R.id.btnSearch)
        btnSearch.setOnClickListener { cherchePersonne("%in%") }

        // création du ViewModel et abonnement
        mainActivityViewModel = ViewModelProvider(this).get(
            MainActivityViewModel::class.java
        )
        abonnementViewModel()

        // méthode pour tester la BDD (ajout unqiue ou multiple, simple ou compexe, ...).
        // N'oubliez pas que vous pouvez consulter la BDD en live avec l'onglet App Inspection (qui
        // demande un certain temps avant d'afficher la BDD...)
        testRoom()
    }

    private fun cherchePersonne(mots: String) {
        mainActivityViewModel!!.cherchePersonne(mots)
    }

    private fun abonnementViewModel() {

        // abonnement aux données exposées par le ViewModel afin de répercuter des changements de
        // valeur automatiquement sur l'IHM (choix arbitraire de ma part. Cf. le viewmodel pour
        // explications)
        // Ici, on s'abonne au 'getAllPersons()'
//        mainActivityViewModel.getPersonsAndProjects().observe(this, new Observer<List<PersonAndProjects>>() {
//            @Override
//            public void onChanged(List<PersonAndProjects> personList) {
//                // Traitement à appliquer chaque fois que les données du viewmodel changent.
//                // Ici, ce n'est pas du tout optimisé (et que textuel). Avec un RecyclerView,
//                // on pourrait utiliser un DiffUtils par exemple pour savoir quelles données ont été
//                // ajoutées/modifiées/supprimées, et réagir en conséquence.
//                afficheListePersonnesEtProjets(personList);
//            }
//        });
        mainActivityViewModel!!.persons.observe(this) { personList -> // Traitement à appliquer chaque fois que les données du viewmodel changent.
            // Ici, ce n'est pas du tout optimisé (et que textuel). Avec un RecyclerView,
            // on pourrait utiliser un DiffUtils par exemple pour savoir quelles données ont été
            // ajoutées/modifiées/supprimées, et réagir en conséquence.
            afficheListePersonnes(personList)
        }
    }

    // affichage des données dans une zone texte
    private fun afficheListePersonnes(personList: List<Person>) {
        Log.d("JC", "observer liste de personnes : ")
        listeDesPersonnes!!.text = ""
        for (person in personList) {
            Log.d("JC", person.name!!)
            listeDesPersonnes!!.text = StringBuilder()
                .append(listeDesPersonnes!!.text.toString())
                .append(System.getProperty("line.separator"))
                .append(person.name)
        }
    }

    // affichage des données dans une zone texte
    private fun afficheListePersonnesEtMaison(personList: List<PersonAndHouse>) {
        if (!personList.isEmpty()) {
            Log.d("JC", "observer liste de personnes : ")
            listeDesPersonnes!!.text = ""
            for (person in personList) {
                Log.d("JC", person.owner!!.name!!)
                if (person.house != null) {
                    listeDesPersonnes!!.text = StringBuilder()
                        .append(listeDesPersonnes!!.text.toString())
                        .append(System.getProperty("line.separator"))
                        .append(person.owner!!.name)
                        .append(" habite à : ")
                        .append(person.house!!.name)
                } else Log.d("JC", "maison vide")
            }
        } else Log.d("JC", "Liste vide....")
    }

    // affichage des données dans une zone texte
    private fun afficheListePersonnesEtAnimaux(personList: List<PersonAndPets>) {
        if (!personList.isEmpty()) {
            Log.d("JC", "observer liste de personnes : ")
            listeDesPersonnes!!.text = ""
            for (person in personList) {
                Log.d("JC", person.person!!.name!!)
                if (!person.pets!!.isEmpty()) {
                    listeDesPersonnes!!.text = StringBuilder()
                        .append(listeDesPersonnes!!.text.toString())
                        .append(System.getProperty("line.separator"))
                        .append(System.getProperty("line.separator"))
                        .append(person.person!!.name)
                        .append(" a comme animal : ")
                    var listePets = StringBuilder()
                    for (pet in person.pets!!) {
                        listePets = StringBuilder()
                            .append(listePets)
                            .append(pet.name)
                            .append(", ")
                    }
                    listeDesPersonnes!!.text = StringBuilder()
                        .append(listeDesPersonnes!!.text.toString())
                        .append(listePets)
                } else Log.d("JC", "pas d'animal")
            }
        } else Log.d("JC", "Liste vide....")
    }

    // affichage des données dans une zone texte
    private fun afficheListePersonnesEtProjets(personList: List<PersonAndProjects>) {
        if (!personList.isEmpty()) {
            Log.d("JC", "observer liste de personnes : ")
            listeDesPersonnes!!.text = ""
            for (person in personList) {
                Log.d("JC", person.person!!.name!!)
                if (!person.projects!!.isEmpty()) {
                    listeDesPersonnes!!.text = StringBuilder()
                        .append(listeDesPersonnes!!.text.toString())
                        .append(System.getProperty("line.separator"))
                        .append(System.getProperty("line.separator"))
                        .append(person.person!!.name)
                        .append(" a comme projet : ")
                    var listeProjects = StringBuilder()
                    for (project in person.projects!!) {
                        listeProjects = StringBuilder()
                            .append(listeProjects)
                            .append(project.name)
                            .append(", ")
                    }
                    listeDesPersonnes!!.text = StringBuilder()
                        .append(listeDesPersonnes!!.text.toString())
                        .append(listeProjects)
                } else Log.d("JC", "pas de projet")
            }
        } else Log.d("JC", "Liste vide....")
    }

    private fun testRoom() {
        createData()
    }

    private fun createData() {
        // ajout de Personnes simples (pas de Maison ou autre)
        ajouterPersonne("Achile Talon")
        ajouterPersonne("Tintin")
        ajouterPersonne("Haddock")

        // ajout de Personnes avec leur maison
//        ajouterPersonneEtMaison("Achile Talon", "Maison Blanche");
//        ajouterPersonneEtMaison("Tintin", "Moulinsart");
//        ajouterPersonneEtMaison("Haddock", "Vieux bateau");

        // ajout de Personnes avec leur maison
//        ajouterPersonneEtAnimaux("Achile Talon");
//        ajouterPersonneEtAnimaux("Tintin");
//        ajouterPersonneEtAnimaux("Haddock");

        // ajout de Personnes avec leur maison
//        ajouterPersonneEtProjets("Achile Talon");
//        ajouterPersonneEtProjets("Tintin");
//        ajouterPersonneEtProjets("Haddock");
    }

    private fun ajouterPersonne(namePerson: String) {
        // Ajoute une Person
        val p = Person(namePerson)
        mainActivityViewModel!!.addPerson(p)
    }

    private fun ajouterPersonneEtMaison(namePerson: String, nameHouse: String) {
        // Ajoute une Person et sa House
        val p = Person(namePerson)
        // On crée la maison por la personne (pour simplifier le code, on ne fait pas de vérification de doublon potentiel)
        val house = House(nameHouse)
        // on affecte la maison à la personne
        p.house = house
        // on ajoute le tout à la BDD (pour rappel, la maison N'EST PAS stockée dans la table Person)
        mainActivityViewModel!!.addPersonAndHouse(p)
    }

    private fun ajouterPersonneEtAnimaux(namePerson: String) {
        // Ajoute une Person et ses animaux
        val p = Person(namePerson)
        // On crée les animaux et on les affecte à la Personne
        for (i in 1..3) {
            val pet = Pet("Animal ($namePerson) $i")
            //TODO trouver l'équivalment en Kotlin
            p.pets.add(pet)
            // on ajoute le tout à la BDD (pour rappel, la maison N'EST PAS stockée dans la table Person)
        }
        mainActivityViewModel!!.addPersonAndPets(p)
    }

    private fun ajouterPersonneEtProjets(namePerson: String) {
        // Ajoute une Person et ses Projets
        val p = Person(namePerson)
        // On crée les projets et on les affecte à la Personne
        for (i in 1..2) {
            val project = Project("Projet-" + p.name + " " + i)
            //TODO trouver l'équivalment en Kotlin
            p.projects.add(project)
            // on ajoute le tout à la BDD (pour rappel, le projet N'EST PAS stocké dans la table Person)
        }
        mainActivityViewModel!!.addPersonAndProjects(p)
    }
}