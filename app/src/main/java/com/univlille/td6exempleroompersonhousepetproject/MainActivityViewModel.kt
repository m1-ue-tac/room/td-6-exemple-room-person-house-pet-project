package com.univlille.td6exempleroompersonhousepetproject

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.univlille.td6exempleroompersonhousepetproject.data.My_Repository
import com.univlille.td6exempleroompersonhousepetproject.data.entity.Person
import com.univlille.td6exempleroompersonhousepetproject.data.model.PersonAndHouse
import com.univlille.td6exempleroompersonhousepetproject.data.model.PersonAndPets
import com.univlille.td6exempleroompersonhousepetproject.data.model.PersonAndProjects
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class MainActivityViewModel(application: Application) : AndroidViewModel(application) {
    // les données qui seront exposées aux observateurs (les Vues)
    private val allPersons: MutableLiveData<List<Person>>
    private val allPersonsAndHouse: MutableLiveData<List<PersonAndHouse>>
    private val allPersonsAndPets: MutableLiveData<List<PersonAndPets>>
    private val allPersonsAndProjects: MutableLiveData<List<PersonAndProjects>>

    //le repository
    private var repository: My_Repository? = null

    // Constructeur avec un contexte en paramètre.
    // On utilise ce contexte pour créer le repository
    init {

        // liste des personnes 'exposées' à la Vue (l'activité dans le cas présent)
        allPersons = MutableLiveData()
        allPersonsAndHouse = MutableLiveData()
        allPersonsAndPets = MutableLiveData()
        allPersonsAndProjects = MutableLiveData()

        // création du repository
        // (pour éviter cette 'dépendance', on pourra utiliser de l'injection de dépendance avec
        // Hilt (ou Dagger2)
        createRepository(application)

        // on requête tout de suite la BDD pour fournir des données à la vue (pas obligatoire de le
        // faire ici, peut être fait + tard dans le code)
//        getAllPersonsAndProjects();
        getAllPersons()
    }

    private fun createRepository(application: Application) {
        repository = My_Repository(application)
    }

    // methode qui demande la liste de toutes les Person au repository, puis qui transforme le
    // résultat (flux Rx Single) en LiveData (données exposées en lecture seule aux vues)
    // On utilise un Observable pour prendre en compte immédiatement les màj de la BDD,
    // cf. https://medium.com/androiddevelopers/room-rxjava-acb0cd4f3757 pour explications

    // TODO passer de Rx à Flow/State en Kotlin

    fun getAllPersons() {
        val persons = repository!!.allPersons
        val observer: Observer<List<Person>> = persons!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(object : Observer<List<Person>> {
                override fun onSubscribe(d: Disposable) {
                    Log.d("JC", "subscribe ON")
                }

                override fun onNext(personList: List<Person>) {
                    Log.d("JC", "next")
                    // chaque fois qu'on reçoit des données, on les convertit en LiveData et
                    // comme ces LiveData sont écoutées par la Vue, cette dernière sera mise
                    // à jour automatiquement. C'est un choix que j'ai fait ici, mais on pourrait
                    // tout à fait utiliser une autre approche, par exemple décider de ne pas
                    // utiliser de LiveDate et faire que le Vue requête le ViewModel quand
                    // elle le souhaite pour se mettre à jour.
                    allPersons.setValue(personList)
                }

                override fun onError(e: Throwable) {
                    Log.d("JC", "ERREUR : " + e.message)
                }

                override fun onComplete() {
                    Log.d("JC", "complete")
                }
            })
    }

    // méthode qui fournit les données (Personnes uniquement) pour que la Vue puisse s'y abonner et réagir en cas de
    // modification de ces données
    val persons: LiveData<List<Person>>
        get() = allPersons

    // on requête la BDD (par l'intermédiaire du repository) pour ajouter une Person
    fun addPerson(person: Person?) {
        repository!!.insert(person)
    }

    // on requête la BDD (par l'intermédiaire du repository) pour ajouter une Person et sa House
    fun addPersonAndHouse(person: Person?) {
        repository!!.addPersonAndHouse(person!!)
    }

    // on requête la BDD (par l'intermédiaire du repository) pour ajouter une Person et ses animaux
    fun addPersonAndPets(person: Person?) {
        repository!!.addPersonAndPets(person!!)
    }

    // on requête la BDD (par l'intermédiaire du repository) pour ajouter une Person et ses Projets
    fun addPersonAndProjects(person: Person?) {
        repository!!.addPersonAndProjects(person!!)
    }

    // méthode qui fournit les données (Pesonnes et leur Maison) pour que la Vue puisse s'y abonner et réagir en cas de
    // modification de ces données
    val personsAndHouse: LiveData<List<PersonAndHouse>>
        get() = allPersonsAndHouse

    // methode qui demande la liste de toutes les Person au repository, puis qui transforme le
    // résultat (flux Rx Single) en LiveData (données exposées en lecture seule aux vues)
    // On utilise un Observable pour prendre en compte immédiatement les màj de la BDD,
    // cf. https://medium.com/androiddevelopers/room-rxjava-acb0cd4f3757 pour explications

    // TODO passer de Rx à Flow/State en Kotlin

    fun getAllPersonsAndHouse() {
        val persons = repository!!.allPersonsAndHouse
        val observer: Observer<List<PersonAndHouse>> = persons!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(object : Observer<List<PersonAndHouse>> {
                override fun onSubscribe(d: Disposable) {
                    Log.d("JC", "subscribe ON")
                }

                override fun onNext(personList: List<PersonAndHouse>) {
                    Log.d("JC", "next")
                    // chaque fois qu'on reçoit des données, on les convertit en LiveData et
                    // comme ces LiveData sont écoutées par la Vue, cette dernière sera mise
                    // à jour automatiquement. C'est un choix que j'ai fait ici, mais on pourrait
                    // tout à fait utiliser une autre approche, par exemple décider de ne pas
                    // utiliser de LiveDate et faire que le Vue requête le ViewModel quand
                    // elle le souhaite pour se mettre à jour.
                    allPersonsAndHouse.setValue(personList)
                }

                override fun onError(e: Throwable) {
                    Log.d("JC", "ERREUR : " + e.message)
                }

                override fun onComplete() {
                    Log.d("JC", "complete")
                }
            })
    }

    // méthode qui fournit les données (Pesonnes et leur Maison) pour que la Vue puisse s'y abonner et réagir en cas de
    // modification de ces données
    val personsAndPets: LiveData<List<PersonAndPets>>
        get() = allPersonsAndPets

    // methode qui demande la liste de toutes les Person au repository, puis qui transforme le
    // résultat (flux Rx Single) en LiveData (données exposées en lecture seule aux vues)
    // On utilise un Observable pour prendre en compte immédiatement les màj de la BDD,
    // cf. https://medium.com/androiddevelopers/room-rxjava-acb0cd4f3757 pour explications

    // TODO passer de Rx à Flow/State en Kotlin

    fun getAllPersonsAndPets() {
        val persons = repository!!.allPersonsAndPets
        val observer: Observer<List<PersonAndPets>> = persons!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(object : Observer<List<PersonAndPets>> {
                override fun onSubscribe(d: Disposable) {
                    Log.d("JC", "subscribe ON")
                }

                override fun onNext(personList: List<PersonAndPets>) {
                    Log.d("JC", "next")
                    // chaque fois qu'on reçoit des données, on les convertit en LiveData et
                    // comme ces LiveData sont écoutées par la Vue, cette dernière sera mise
                    // à jour automatiquement. C'est un choix que j'ai fait ici, mais on pourrait
                    // tout à fait utiliser une autre approche, par exemple décider de ne pas
                    // utiliser de LiveDate et faire que le Vue requête le ViewModel quand
                    // elle le souhaite pour se mettre à jour.
                    allPersonsAndPets.setValue(personList)
                }

                override fun onError(e: Throwable) {
                    Log.d("JC", "ERREUR : " + e.message)
                }

                override fun onComplete() {
                    Log.d("JC", "complete")
                }
            })
    }

    // méthode qui fournit les données (Pesonnes et leurs projets) pour que la Vue puisse s'y abonner et réagir en cas de
    // modification de ces données
    val personsAndProjects: LiveData<List<PersonAndProjects>>
        get() = allPersonsAndProjects

    // methode qui demande la liste de toutes les Person au repository, puis qui transforme le
    // résultat (flux Rx Single) en LiveData (données exposées en lecture seule aux vues)
    // On utilise un Observable pour prendre en compte immédiatement les màj de la BDD,
    // cf. https://medium.com/androiddevelopers/room-rxjava-acb0cd4f3757 pour explications

    // TODO passer de Rx à Flow/State en Kotlin

    fun getAllPersonsAndProjects() {
        val persons = repository!!.allPersonsAndProjects
        val observer: Observer<List<PersonAndProjects>> = persons!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(object : Observer<List<PersonAndProjects>> {
                override fun onSubscribe(d: Disposable) {
                    Log.d("JC", "subscribe ON")
                }

                override fun onNext(personList: List<PersonAndProjects>) {
                    Log.d("JC", "next")
                    // chaque fois qu'on reçoit des données, on les convertit en LiveData et
                    // comme ces LiveData sont écoutées par la Vue, cette dernière sera mise
                    // à jour automatiquement. C'est un choix que j'ai fait ici, mais on pourrait
                    // tout à fait utiliser une autre approche, par exemple décider de ne pas
                    // utiliser de LiveDate et faire que le Vue requête le ViewModel quand
                    // elle le souhaite pour se mettre à jour.
                    allPersonsAndProjects.setValue(personList)
                }

                override fun onError(e: Throwable) {
                    Log.d("JC", "ERREUR : " + e.message)
                }

                override fun onComplete() {
                    Log.d("JC", "complete")
                }
            })
    }


    // TODO passer de Rx à Flow/State en Kotlin

    fun cherchePersonne(mots: String?) {
        val persons = repository!!.cherchePersonne(mots)
        val observer: Observer<List<Person>> = persons!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(object : Observer<List<Person>> {
                override fun onSubscribe(d: Disposable) {
                    Log.d("JC", "subscribe RECHERCHE ON")
                }

                override fun onNext(personList: List<Person>) {
                    Log.d("JC", "next")
                    // chaque fois qu'on reçoit des données, on les convertit en LiveData et
                    // comme ces LiveData sont écoutées par la Vue, cette dernière sera mise
                    // à jour automatiquement. C'est un choix que j'ai fait ici, mais on pourrait
                    // tout à fait utiliser une autre approche, par exemple décider de ne pas
                    // utiliser de LiveDate et faire que le Vue requête le ViewModel quand
                    // elle le souhaite pour se mettre à jour.
                    allPersons.setValue(personList)
                }

                override fun onError(e: Throwable) {
                    Log.d("JC", "ERREUR : " + e.message)
                }

                override fun onComplete() {
                    Log.d("JC", "RECHERCHE complete")
                }
            })
    }
}