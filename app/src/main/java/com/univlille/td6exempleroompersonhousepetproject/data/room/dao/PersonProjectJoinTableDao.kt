package com.univlille.td6exempleroompersonhousepetproject.data.room.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.univlille.td6exempleroompersonhousepetproject.data.entity.PersonProjectJoinTable

// Cf. remarques générales dans PersonDao
@Dao
interface PersonProjectJoinTableDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(personProjectJoinTable: PersonProjectJoinTable?)

    @Query("DELETE FROM PersonProjectJoinTable")
    fun deleteAll()

    @get:Query("SELECT * FROM PersonProjectJoinTable")
    val allPersonProjectJoinTables: LiveData<List<PersonProjectJoinTable?>?>?
}