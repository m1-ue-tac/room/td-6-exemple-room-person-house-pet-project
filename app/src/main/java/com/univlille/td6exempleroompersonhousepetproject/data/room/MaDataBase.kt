package com.univlille.td6exempleroompersonhousepetproject.data.room

import android.content.Context
import android.util.Log
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.univlille.td6exempleroompersonhousepetproject.data.entity.*
import com.univlille.td6exempleroompersonhousepetproject.data.room.dao.*
import java.util.concurrent.Executors

// chaque fois qu'on modifie la structure de la BDD, il ne faut pas oublier de changer le numéro de version
// et faire la migration (ou supprimer automatiquement la BDD dans le constructeur avec .fallbackToDestructiveMigration()
// --> ainsi on garde la compatibilité avec toutes les installations existantes sur d'autres téléphones
//
// Pour des tests en local, une autre solution consiste à désinstaller l'application sur le téléphone chaque fois avant de
// lancer une nouvelle version.
// --> ne garde pas la compatibilié avec l'existant dont UNIQUEMENT POUR TEST LOCAL
@Database(
    entities = [Person::class, House::class, Pet::class, Project::class, PersonProjectJoinTable::class],
    version = 1,
    exportSchema = false
)
abstract class MaDataBase : RoomDatabase() {
    abstract fun personDao(): PersonDao?
    abstract fun houseDao(): HouseDao?
    abstract fun petDao(): PetDao?
    abstract fun projectDao(): ProjectDao?
    abstract fun personProjectJoinTableDao(): PersonProjectJoinTableDao?

    companion object {
        @Volatile
        private var INSTANCE: MaDataBase? = null
        private const val NUMBER_OF_THREADS = 4
        @JvmField
        val databaseWriteExecutor = Executors.newFixedThreadPool(NUMBER_OF_THREADS)
        @JvmStatic
        fun getDatabase(context: Context): MaDataBase? {

            // uniquement pour les tests, on vide la BDD à chaque fois (run from scratch), donc pas besoin
            // de gérer le changement de numéro de version
            // Ne pas oublier de retirer cette ligne à la fin !!!!
            if (context.deleteDatabase("madatabase")) Log.d(
                "JC",
                "RESET de la BDD OK"
            ) else Log.d("JC", "RESET de la BDD PAS OK !!!")
            if (INSTANCE == null) {
                synchronized(MaDataBase::class.java) {
                    if (INSTANCE == null) {
                        INSTANCE = Room.databaseBuilder(
                            context.applicationContext,
                            MaDataBase::class.java, "madatabase"
                        ) // si on veut pré-remplir la BDD (code à finir)
                            .addCallback(sRoomDatabaseCallback) // si on veut tout supprimer lors d'une migration
                            .fallbackToDestructiveMigration()
                            .build()
                    }
                }
            }
            return INSTANCE
        }

        /**
         * Plutôt que de créer les données dans l'activité, on pourrait tout à fait le faire ici.
         */
        private val sRoomDatabaseCallback: Callback = object : Callback() {
            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)
                databaseWriteExecutor.execute {
                    // Populate the database in the background.
                    // If you want to start with more "Truc", just add them.
//                TRUC_Dao dao = INSTANCE.trucDao();
//                dao.deleteAll();
//
//                Truc truc = new Truc("machin");
//                dao.insert(truc);
//                truc = new Truc("bidule");
//                dao.insert(truc);
                    Log.d("JC", "prépopulate de la base")
                }
            }
        }
    }
}