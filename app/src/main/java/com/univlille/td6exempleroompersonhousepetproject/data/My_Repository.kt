package com.univlille.td6exempleroompersonhousepetproject.data

import android.app.Application
import com.univlille.td6exempleroompersonhousepetproject.data.entity.Person
import com.univlille.td6exempleroompersonhousepetproject.data.entity.PersonProjectJoinTable
import com.univlille.td6exempleroompersonhousepetproject.data.model.PersonAndHouse
import com.univlille.td6exempleroompersonhousepetproject.data.model.PersonAndPets
import com.univlille.td6exempleroompersonhousepetproject.data.model.PersonAndProjects
import com.univlille.td6exempleroompersonhousepetproject.data.room.MaDataBase
import com.univlille.td6exempleroompersonhousepetproject.data.room.MaDataBase.Companion.getDatabase
import com.univlille.td6exempleroompersonhousepetproject.data.room.dao.HouseDao
import com.univlille.td6exempleroompersonhousepetproject.data.room.dao.PersonDao
import com.univlille.td6exempleroompersonhousepetproject.data.room.dao.PetDao
import com.univlille.td6exempleroompersonhousepetproject.data.room.dao.ProjectDao
import io.reactivex.Observable

class My_Repository(application: Application?) {
    private val db: MaDataBase?

    // en prévision des requêtes ultérieures
    private val personDao: PersonDao?
    private val houseDao: HouseDao?
    private val petDao: PetDao?
    private val projectDao: ProjectDao?

    init {

        // récupération de la BDD
        db = getDatabase(application!!)

        // récupération des Dao
        personDao = db!!.personDao()
        houseDao = db.houseDao()
        petDao = db.petDao()
        projectDao = db.projectDao()
    }

    // Comme indiqué en cours, on doit passer par un Executor car les appels du Dao peuvent prendre
    // du temps, ce qui ferait crasher l'appli (contrainte de codage imposée par Android)
    // ajout d'une Person dans la BDD
    fun insert(person: Person?) {
        MaDataBase.databaseWriteExecutor.execute { personDao!!.insert(person) }
    }

    // ajout d'une Person et sa House.
    // Room n'est pas un ORM, on doit donc faire tous les ajouts soi-même. Dans le cas présent,
    // on doit ajouter la personne, puis ajouter la maison en récupérant l'id de la personne associée
    fun addPersonAndHouse(p: Person) {
        MaDataBase.databaseWriteExecutor.execute {

            // on récupère la maison de la personne
            val house = p.house
            // on ajoute la personne et on récupère l'id de cette personne pour l'affecter à la maison
            val id = db!!.personDao()!!.insert(p)
            // on affecte l'id de la personne à sa maison
            house!!.personID = id.toInt().toLong()
            // on insère finalement la maison
            db.houseDao()!!.insert(house)
        }
    }

    // ajout d'une Person et ses animaux.
    // Room n'est pas un ORM, on doit donc faire tous les ajouts soi-même. Dans le cas présent,
    // on doit ajouter la personne, puis ajouter les animaux en récupérant l'id de la personne associée
    fun addPersonAndPets(p: Person) {
        MaDataBase.databaseWriteExecutor.execute {

            // on récupère les animaux de la personne
            val pets = p.pets
            // on ajoute la personne et on récupère l'id de cette personne pour l'affecter à la maison
            val id = db!!.personDao()!!.insert(p)
            // Pour chaque animal, on affecte l'id de la personne à l'animal et on l'insère
            for (pet in pets!!) {
                pet!!.personID = id.toInt().toLong()
                // on insère finalement la maison
                db.petDao()!!.insert(pet)
            }
        }
    }

    fun addPersonAndProjects(p: Person) {
        MaDataBase.databaseWriteExecutor.execute {

            // on récupère les projets de la personne
            val projects = p.projects
            // on ajoute la personne et on récupère l'id de cette personne pour l'affecter à ses projets
            val personId = db!!.personDao()!!.insert(p)
            // pour chaque projet de la personne,
            for (project in projects!!) {
                // on crée le projet et on l'insère dans la BDD.

                // NOTE IMPORTANTE : ici, on devrait vérifier si le projet existe déjà ou non,
                // et si oui, on devrait simplement ajouter la personne au projet. POUR SIMPLIFIER
                // L'EXEMPLE, j'ai choisi de créer chaque fois un nouveau projet.
                val projectId = db.projectDao()!!.insert(project)

                // on ajoute le projet et la personne à la table de jointure
                val join = PersonProjectJoinTable(personId, projectId)
                db.personProjectJoinTableDao()!!.insert(join)
            }
        }
    }

    // Méthode qui renvoie toutes les Person de la BDD, sous la forme d'un flux Rx (qui sera ensuite
    // transformé en LiveData par le ViewModel)
    // Pour le choix entre Observable/Flowable/Single/Maybe/Completable, cf. https://medium.com/androiddevelopers/room-rxjava-acb0cd4f3757
    val allPersons: Observable<List<Person?>?>?
        get() = db!!.personDao()!!.allPersons

    // Méthode qui renvoie toutes les personnes avec leur maion, sous la forme d'un flux Rx (qui
    // sera ensuite transformé en LiveData par le ViewModel)
    val allPersonsAndHouse: Observable<List<PersonAndHouse?>?>?
        get() = db!!.personDao()!!.allPersonsAndHouse

    // Méthode qui renvoie toutes les personnes avec leurs animaux, sous la forme d'un flux Rx (qui
    // sera ensuite transformé en LiveData par le ViewModel)
    val allPersonsAndPets: Observable<List<PersonAndPets?>?>?
        get() = db!!.personDao()!!.allPersonsAndPets

    // Méthode qui renvoie toutes les personnes avec leurs projets, sous la forme d'un flux Rx (qui
    // sera ensuite transformé en LiveData par le ViewModel)
    val allPersonsAndProjects: Observable<List<PersonAndProjects?>?>?
        get() = db!!.personDao()!!.allPersonsAndProjects

    fun cherchePersonne(mots: String?): Observable<List<Person?>?>? {
        return db!!.personDao()!!.cherchePersonne(mots)
    }
}