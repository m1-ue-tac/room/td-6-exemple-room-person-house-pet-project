package com.univlille.td6exempleroompersonhousepetproject.data.room.dao

import androidx.room.Dao
import androidx.room.Insert
import com.univlille.td6exempleroompersonhousepetproject.data.entity.Pet

// Cf. remarques générales dans PersonDao
@Dao
interface PetDao {
    @Insert
    fun insert(pet: Pet?)
}