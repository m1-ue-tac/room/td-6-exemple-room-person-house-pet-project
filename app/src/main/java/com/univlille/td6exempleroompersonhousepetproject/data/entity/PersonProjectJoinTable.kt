package com.univlille.td6exempleroompersonhousepetproject.data.entity

import androidx.room.ColumnInfo
import androidx.room.Entity

/**
 * Entité représentant la jointure N-N de la BDD (Person-Project)
 * Pour rappel, pour les relations N-N, il faut créer une nouvelle table dans la BDD (donc une Entity)
 * ==> donc NE PAS OUBLIER d'ajouter cette entité à la description de la BDD !!!
 * (cf. @Database(entities =... dans MaDataBase.class)
 */
// clé composée des Id de Person et Project
@Entity(primaryKeys = ["personId", "projectId"])
class PersonProjectJoinTable(personId: Long, projectId: Long) {
    /**
     * Getters/Setters utilisés entre autres par Room pour accéder aux données
     */
    var personId: Long = 0

    // nouveauté de 2022 : Android lève un warning car pas assez optimisé pour la clé ci-dessous
    // message = warning: The column projectId in the junction entity com.univlille.td6exempleroompersonhousepetproject.data.entity.PersonProjectJoinTable
    // is being used to resolve a relationship but it is not covered by any index. This might cause
    // a full table scan when resolving the relationship, it is highly advised to create an index
    // that covers this column.
    //    private long projectId;
    // du coup, ajout d'un index pour enlever le warning.
    // Deux solutions :
    // celle-ci...
    @ColumnInfo(index = true) // ou celle-là en changeant @Entity ci-dessus :
    // @Entity(primaryKeys = {"personId","projectId"},
    //        indices = {
    //        @Index(value = "projectId")
    //        }
    var projectId: Long = 0

    // Constructeur avec les Id de la personne et du projet associé
    init {
        this.personId = personId
        this.projectId = projectId
    }
}