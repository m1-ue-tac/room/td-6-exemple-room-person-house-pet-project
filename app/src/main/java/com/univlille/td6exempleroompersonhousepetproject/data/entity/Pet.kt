package com.univlille.td6exempleroompersonhousepetproject.data.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class Pet(name: String?) {
    /**
     * Getters/Setters utilisés entre autres par Room pour accéder aux données
     */
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
    var personID: Long = 0
    var name: String? = null

    // Constructeur avec un nom d'animal
    init {
        this.name = name
    }
}