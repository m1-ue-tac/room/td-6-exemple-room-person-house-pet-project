package com.univlille.td6exempleroompersonhousepetproject.data.room.dao

import androidx.room.Dao
import androidx.room.Insert
import com.univlille.td6exempleroompersonhousepetproject.data.entity.Project

// Cf. remarques générales dans PersonDao
@Dao
interface ProjectDao {
    @Insert
    fun insert(project: Project?): Long
}