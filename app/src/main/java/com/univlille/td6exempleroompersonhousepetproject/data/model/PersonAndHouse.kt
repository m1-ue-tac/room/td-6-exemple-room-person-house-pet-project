package com.univlille.td6exempleroompersonhousepetproject.data.model

import androidx.room.Embedded
import androidx.room.Relation
import com.univlille.td6exempleroompersonhousepetproject.data.entity.House
import com.univlille.td6exempleroompersonhousepetproject.data.entity.Person

/**
 * classe représentant la jointure 1-1 de la BDD (Person-House)
 */
class PersonAndHouse {
    /**
     * Getters/Setters utilisés entre autres par Room pour accéder aux données
     */
    @Embedded
    var owner: Person? = null

    @Relation(parentColumn = "id", entityColumn = "personID")
    var house: House? = null
}