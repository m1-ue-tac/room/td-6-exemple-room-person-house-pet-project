package com.univlille.td6exempleroompersonhousepetproject.data.room.dao

import androidx.room.*
import com.univlille.td6exempleroompersonhousepetproject.data.entity.Person
import com.univlille.td6exempleroompersonhousepetproject.data.model.PersonAndHouse
import com.univlille.td6exempleroompersonhousepetproject.data.model.PersonAndPets
import com.univlille.td6exempleroompersonhousepetproject.data.model.PersonAndProjects
import io.reactivex.Observable

@Dao
interface PersonDao {
    // De même que le getAllPersons() renvoie un Observable, on pourrait utiliser ici un Single,
    // Maybe, ou Completable. Cf. https://medium.com/androiddevelopers/room-rxjava-acb0cd4f3757
    // et du coup il faudrait modifier le ViewModel de la même façon que cela a été fait pour le
    // getAllPersons()
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(person: Person?): Long

    // De même que le getAllPersons() renvoie un Observable, on pourrait utiliser ici un Single,
    // Maybe, ou Completable. Cf. https://medium.com/androiddevelopers/room-rxjava-acb0cd4f3757
    // et du coup il faudrait modifier le ViewModel de la même façon que cela a été fait pour le
    // getAllPersons()
    @Query("DELETE FROM Person")
    fun deleteAll()

    // Ici, suivant ce que l'on veut récupérer comme data, le format, et l'interaction voulue sur
    // la Vue, on peut utiliser un Observable (comme ici), un Single, ou un MayBe.
    // Cf. https://medium.com/androiddevelopers/room-rxjava-acb0cd4f3757
    @get:Query("SELECT * FROM Person")
    val allPersons: Observable<List<Person?>?>?

    // Requête pour récupérer la liste des Personnes avec leur Maison.
    // On utilise la classe PersonAndHouse pour récupérer la jointure
    @get:Query("SELECT * FROM Person")
    @get:Transaction
    val allPersonsAndHouse: Observable<List<PersonAndHouse?>?>?

    // Requête pour récupérer la liste des Personnes avec leurs Animaux.
    // On utilise la classe PersonAndPets pour récupérer la jointure
    @get:Query("SELECT * FROM Person")
    @get:Transaction
    val allPersonsAndPets: Observable<List<PersonAndPets?>?>?

    // Requête pour récupérer la liste des Personnes avec leurs Projets.
    // On utilise la classe PersonAndProject pour récupérer la jointure
    @get:Query("SELECT * FROM Person")
    @get:Transaction
    val allPersonsAndProjects: Observable<List<PersonAndProjects?>?>?

    @Query("SELECT * FROM Person WHERE name LIKE :mots")
    fun cherchePersonne(mots: String?): Observable<List<Person?>?>?
}