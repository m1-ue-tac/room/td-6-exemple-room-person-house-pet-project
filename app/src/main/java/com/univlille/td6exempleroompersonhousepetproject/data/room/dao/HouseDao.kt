package com.univlille.td6exempleroompersonhousepetproject.data.room.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.univlille.td6exempleroompersonhousepetproject.data.entity.House

// Cf. remarques générales dans PersonDao
@Dao
interface HouseDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(house: House?)

    @Query("DELETE FROM House")
    fun deleteAll()

    @get:Query("SELECT * FROM House")
    val allHouses: LiveData<List<House?>?>?
}