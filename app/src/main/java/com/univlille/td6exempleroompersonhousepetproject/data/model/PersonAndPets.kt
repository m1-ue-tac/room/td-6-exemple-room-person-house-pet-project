package com.univlille.td6exempleroompersonhousepetproject.data.model

import androidx.room.Embedded
import androidx.room.Relation
import com.univlille.td6exempleroompersonhousepetproject.data.entity.Person
import com.univlille.td6exempleroompersonhousepetproject.data.entity.Pet

/**
 * classe représentant la jointure 1-N de la BDD (Person-Pets)
 */
class PersonAndPets {
    @JvmField
    @Embedded
    var person: Person? = null

    @JvmField
    @Relation(parentColumn = "id", entityColumn = "personID")
    var pets: List<Pet>? = null
}