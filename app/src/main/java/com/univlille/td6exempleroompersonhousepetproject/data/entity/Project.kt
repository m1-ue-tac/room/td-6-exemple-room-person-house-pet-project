package com.univlille.td6exempleroompersonhousepetproject.data.entity

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity
class Project(name: String?) {
    /**
     * Getters/Setters utilisés entre autres par Room pour accéder aux données
     */
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
    var name: String? = null

    /**
     * Champs ignorés pour le stockage dans la BDD.
     * En effet, on ne va pas stocker les données de House, Pets, etc. dans la table Person puisqu'on
     * peut retrouver ces données grâce aux relations (et requêtes associées).
     * Cela pourrait être éventuellement une solution pour optimiser les temps de requêtes (sans
     * passer par les relations du coup), mais cela augmenterait la taille de la BDD.
     */
    @Ignore
    var persons: List<Person?>? = null

    // Constructeur avec initialisation des Persons (pour simplifier le code)
    init {
        this.name = name
        persons = ArrayList()
    }
}