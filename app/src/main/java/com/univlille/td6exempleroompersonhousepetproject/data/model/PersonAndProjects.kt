package com.univlille.td6exempleroompersonhousepetproject.data.model

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation
import com.univlille.td6exempleroompersonhousepetproject.data.entity.Person
import com.univlille.td6exempleroompersonhousepetproject.data.entity.PersonProjectJoinTable
import com.univlille.td6exempleroompersonhousepetproject.data.entity.Project

/**
 * classe représentant la jointure N-N de la BDD (Person-Projects)
 */
class PersonAndProjects {
    @JvmField
    @Embedded
    var person: Person? = null

    @JvmField
    @Relation(
        parentColumn = "id",
        entityColumn = "id",
        associateBy = Junction(
            value = PersonProjectJoinTable::class,
            parentColumn = "personId",
            entityColumn = "projectId"
        )
    )
    var projects: List<Project>? = null
}